package de.hshl.isd.placeholderposts.core

import de.hshl.isd.placeholderposts.common.CommandHandler
import de.hshl.isd.placeholderposts.common.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GetPostCommand(private val repository: Repository<Int, PostEntity>) :
    CommandHandler<GetPostCommandDTO, String> {
    override fun execute(
        inDTO: GetPostCommandDTO,
        successHandler: (success: String) -> Unit,
        errorHandler: (error: Throwable) -> Unit
    ) {
        val scope = MainScope()
        scope.launch {
            withContext(Dispatchers.IO) {
                val result = repository.retrieve(inDTO.id)
                withContext(Dispatchers.Main) {
                    result.onSuccess { successHandler(it.toString()) }
                    result.onFailure { errorHandler(it) }
                }
            }
        }
    }
}