package de.hshl.isd.navigationcompose

import androidx.compose.Composable
import androidx.ui.foundation.Icon
import androidx.ui.foundation.Text
import androidx.ui.material.DrawerState
import androidx.ui.material.IconButton
import androidx.ui.material.ScaffoldState
import androidx.ui.material.TopAppBar
import androidx.ui.res.vectorResource

@Composable
fun DrawerTopAppBar(title: String, scaffoldState: ScaffoldState) {
    TopAppBar(
        title = { Text(text = title) },
        navigationIcon = {
            IconButton(onClick = {
                scaffoldState.drawerState = DrawerState.Opened
            }) {
                Icon(vectorResource(R.drawable.ic_baseline_menu_24))
            }
        }
    )
}