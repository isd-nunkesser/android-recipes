package de.hshl.isd.hellokmm.shared

expect class Platform() {
    val platform: String
}