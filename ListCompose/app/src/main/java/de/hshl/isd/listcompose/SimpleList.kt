package de.hshl.isd.listcompose

import androidx.compose.Composable
import androidx.ui.foundation.ScrollableColumn

@Composable
fun SimpleList(items: List<ItemViewModel>) {
    ScrollableColumn {
        items.forEach { item ->
            ItemRow(item)
        }
    }
}
