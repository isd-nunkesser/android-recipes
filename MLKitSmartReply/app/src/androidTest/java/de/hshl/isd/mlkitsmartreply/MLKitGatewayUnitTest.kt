package de.hshl.isd.mlkitsmartreply

import androidx.test.runner.AndroidJUnit4
import com.google.firebase.ml.naturallanguage.smartreply.FirebaseTextMessage
import com.google.firebase.ml.naturallanguage.smartreply.SmartReplySuggestion
import de.hshl.isd.basiccleanarch.Response
import kotlinx.coroutines.runBlocking
import org.junit.Assert.fail
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class MLKitGatewayUnitTest {
    val gateway = MLKitGateway()

    @Test
    fun testSuggestRepliesFirstMessage() {
        val messages = listOf(
            FirebaseTextMessage.createForRemoteUser(
                "Hello. How are you?",
                1568128019024,
                "REMOTE_USER_ID"
            )
        )
        runBlocking {
            val result = gateway.suggestReplies(messages)
            when (result) {
                is Response.Success<*> -> {
                    val replies = result.value as List<SmartReplySuggestion>
                    assert(!replies.isEmpty())
                }
                is Response.Failure -> {
                    fail(result.toString())
                }
            }
        }
    }

    @Test
    fun testSuggestRepliesBasicContent() {
        val messages = listOf(
            FirebaseTextMessage.createForLocalUser("Hello", 1568128019024),
            FirebaseTextMessage.createForRemoteUser("Hey", 1568128029024, "REMOTE_USER_ID")
        )
        runBlocking {
            val result = gateway.suggestReplies(messages)
            when (result) {
                is Response.Success<*> -> {
                    val replies = result.value as List<SmartReplySuggestion>
                    assert(!replies.isEmpty())
                }
                is Response.Failure -> {
                    fail(result.toString())
                }
            }
        }
    }

    @Test
    fun testSuggestRepliesSensitiveContent() {
        val messages = listOf(
            FirebaseTextMessage.createForRemoteUser("Hi", 1568128019024, "REMOTE_USER_ID"),
            FirebaseTextMessage.createForLocalUser("How are you?", 1568128029024),
            FirebaseTextMessage.createForRemoteUser("My cat died", 1568128039024, "REMOTE_USER_ID")
        )
        runBlocking {
            val result = gateway.suggestReplies(messages)
            when (result) {
                is Response.Success<*> -> {
                    val replies = result.value as List<SmartReplySuggestion>
                    assert(replies.isEmpty())
                }
                is Response.Failure -> {
                    fail(result.toString())
                }
            }
        }
    }

}