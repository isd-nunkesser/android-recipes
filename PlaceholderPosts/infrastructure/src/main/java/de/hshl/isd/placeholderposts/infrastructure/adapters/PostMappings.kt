package de.hshl.isd.placeholderposts.infrastructure.adapters

import de.hshl.isd.placeholderposts.core.PostEntity
import de.hshl.isd.placeholderposts.infrastructure.Post

fun Post.toPostEntity(): PostEntity {
    return PostEntity(this.userId, this.id, this.title, this.body)
}

fun PostEntity.toPost(): Post {
    return Post(this.userID, this.id, this.title, this.body)
}