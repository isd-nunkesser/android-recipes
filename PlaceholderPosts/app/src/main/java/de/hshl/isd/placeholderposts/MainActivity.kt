package de.hshl.isd.placeholderposts

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.getValue
import androidx.compose.setValue
import androidx.compose.state
import androidx.ui.core.setContent
import androidx.ui.foundation.Text
import androidx.ui.foundation.TextField
import androidx.ui.input.TextFieldValue
import androidx.ui.layout.Column
import androidx.ui.material.Button
import androidx.ui.material.MaterialTheme
import androidx.ui.tooling.preview.Preview
import de.hshl.isd.placeholderposts.core.GetPostCommand
import de.hshl.isd.placeholderposts.core.GetPostCommandDTO
import de.hshl.isd.placeholderposts.infrastructure.adapters.PostRepositoryAdapter

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                MainContent()
            }
        }
    }
}

@Composable
fun MainContent() {
    val tag = "MainContent"
    var id by state { TextFieldValue("1") }
    var resultText by state { "" }
    var service = GetPostCommand(PostRepositoryAdapter())


    fun success(value: String) {
        resultText = value
    }

    fun failure(error: Throwable) {
        Log.e(tag, error.localizedMessage!!)
    }

    Column {
        TextField(
            value = id,
            onValueChange = {
                id = it
            }
        )
        Button(onClick = {
            service.execute(
                GetPostCommandDTO(id.text.toInt()),
                ::success,
                ::failure
            )
        }) {
            Text("Start")
        }
        Text(resultText)
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MaterialTheme {
        MainContent()
    }
}