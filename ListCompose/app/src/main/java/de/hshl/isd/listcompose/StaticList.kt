package de.hshl.isd.listcompose

import androidx.compose.Composable
import androidx.ui.foundation.ScrollableColumn
import androidx.ui.foundation.Text

@Composable
fun StaticList() {
    ScrollableColumn {
        Text(text = "Item 1")
        Text(text = "Item 2")
        Text(text = "Item 3")
    }
}
