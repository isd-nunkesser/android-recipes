package de.hshl.isd.bottomnavigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*

private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity(),
        SecondLevelFragment.OnFragmentInteractionListener {

    private var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        navController = findNavController(R.id.nav_host_fragment)
        setupActionBarWithNavController(navController!!)
        navigation.setupWithNavController(navController!!)
    }

    override fun onSupportNavigateUp() = navController!!.navigateUp()

    override fun onFragmentInteraction(value: String) {
        Log.i(TAG, """Received ${value}""")
    }
}
