package de.hshl.isd.httpbin

data class HttpRequestModel(val origin: String, val url: String)