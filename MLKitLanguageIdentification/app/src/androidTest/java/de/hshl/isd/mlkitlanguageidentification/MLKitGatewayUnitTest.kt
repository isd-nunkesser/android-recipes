package de.hshl.isd.mlkitlanguageidentification

import androidx.test.runner.AndroidJUnit4
import com.google.firebase.ml.naturallanguage.languageid.IdentifiedLanguage
import de.hshl.isd.basiccleanarch.Response
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class MLKitGatewayUnitTest {
    val gateway = MLKitGateway()

    val testDataIdentifyLanguage = listOf(
        Pair("My hovercraft is full of eels.", "en"),
        Pair("Dao shan xue hai", "zh-Latn"),
        Pair("ph'nglui mglw'nafh TensorFlow Google wgah'nagl fhtagn", "und")
    )

    val testDataIdentifyPossibleLanguages = "an amicable coup d'etat"


    @Test
    fun identifyLanguage() {
        for (testItem in testDataIdentifyLanguage) {
            runBlocking {
                val result = gateway.identifyLanguage(testItem.first)
                when (result) {
                    is Response.Success<*> -> {
                        val language = result.value as String
                        assertEquals(testItem.second, language)
                    }
                    is Response.Failure -> {
                        fail(result.toString())
                    }
                }
            }
        }

    }

    @Test
    fun testIdentifyPossibleLanguages() {
        runBlocking {
            val result = gateway.identifyPossibleLanguages(testDataIdentifyPossibleLanguages)
            when (result) {
                is Response.Success<*> -> {
                    val languages = result.value as List<IdentifiedLanguage>
                    assert(!languages.isEmpty())
                }
                is Response.Failure -> {
                    fail(result.toString())
                }
            }
        }
    }
}
