package de.hshl.isd.mlkitlanguageidentification

import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Presenter
import de.hshl.isd.basiccleanarch.Response
import de.hshl.isd.basiccleanarch.UseCase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class IdentifyLanguageInteractor(
    override val presenter: Presenter<String, String>,
    val gateway: MLKitGateway
) : UseCase<TextRequest, String, String> {

    override fun execute(request: TextRequest, displayer: Displayer<String>, requestCode: Int) {
        GlobalScope.async {
            val result = gateway.identifyLanguage(request.text)
            when (result) {
                is Response.Success<*> -> {
                    val language = result.value as String
                    displayer.display(presenter.present(language))
                }
                is Response.Failure -> {
                    displayer.display(result.error)
                }
            }
        }

    }
}