package de.hshl.isd.bottomnavigation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        navButton.setOnClickListener{ view ->
            val bundle = Bundle()
            bundle.putString(ARG_PARAM1, "value1")
            bundle.putString(ARG_PARAM2, "value2")
            findNavController().navigate(
                    R.id.action_homeFragment_to_secondLevelFragment,
                    bundle)
        }
    }

}
