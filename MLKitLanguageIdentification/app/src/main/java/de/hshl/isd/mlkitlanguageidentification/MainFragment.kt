package de.hshl.isd.mlkitlanguageidentification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.ml.naturallanguage.languageid.IdentifiedLanguage
import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.UseCase
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment(), Displayer<String> {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val identifyLanguageInteractor: UseCase<TextRequest, String, String> =
        IdentifyLanguageInteractor(
            IdentifiedLanguagePresenter(),
            MLKitGateway()
        )

    private val identifyPossibleLanguagesInteractor: UseCase<TextRequest, List<IdentifiedLanguage>, String> =
        IdentifyPossibleLanguagesInteractor(
            IdentifiedLanguagesPresenter(),
            MLKitGateway()
        )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        buttonIdLanguage.setOnClickListener {
            identifyLanguageInteractor.execute(TextRequest(editText.text.toString()), this)
        }
        buttonIdAll.setOnClickListener {
            identifyPossibleLanguagesInteractor.execute(TextRequest(editText.text.toString()), this)
        }
    }

    override fun display(success: String, requestCode: Int) {
        Snackbar.make(view!!, success, Snackbar.LENGTH_INDEFINITE).show()
    }

    override fun display(error: Throwable) {
        Snackbar.make(view!!, R.string.language_id_error, Snackbar.LENGTH_INDEFINITE).show()
    }

}
