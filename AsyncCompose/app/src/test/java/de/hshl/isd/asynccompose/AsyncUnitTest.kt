package de.hshl.isd.asynccompose

import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CompletableFuture

class AsyncUnitTest {

    private var testClass: AsyncExample? = null

    @ObsoleteCoroutinesApi
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @ObsoleteCoroutinesApi
    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        testClass = AsyncExample()
    }

    @ObsoleteCoroutinesApi
    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun testSuspendFunctions() {
        assertEquals(runBlocking { testClass!!.doSomethingUsefulOne() }, 13)
        assertEquals(runBlocking { testClass!!.doSomethingUsefulTwo() }, 29)
    }

    @Test
    fun testUIExample() {
        val future = CompletableFuture<Int>()
        testClass!!.uiExample {
            future.complete(1)
        }
        assertEquals(1, future.get() as Int)
    }
}
