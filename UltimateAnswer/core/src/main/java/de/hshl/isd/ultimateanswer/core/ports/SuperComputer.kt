package de.hshl.isd.ultimateanswer.core.ports

interface SuperComputer {
    suspend fun answer(question: String): Result<String>
}