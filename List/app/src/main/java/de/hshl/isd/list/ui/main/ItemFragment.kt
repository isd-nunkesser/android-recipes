package de.hshl.isd.list.ui.main

import androidx.lifecycle.Observer
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import de.hshl.isd.list.R
import kotlinx.android.synthetic.main.fragment_item_list.*


class ItemFragment : Fragment() {

    private lateinit var viewModel: ItemViewModel
    private val adapter = ItemAdapter()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_item_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(ItemViewModel::class.java)
        viewModel.data.observe(this, Observer { adapter.submitList(it) })

        recyclerView.layoutManager =
                LinearLayoutManager(
                        context)
        recyclerView.adapter = adapter

        searchView.setOnQueryTextListener(object :
                SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = false

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.isNullOrBlank()) {
                    adapter.submitList(viewModel.data.value)
                    return false
                }
                val filteredList = viewModel.data.value?.filter {
                    it.contains(newText, true)
                }
                adapter.submitList(filteredList)
                return false
            }
        })

    }
}
