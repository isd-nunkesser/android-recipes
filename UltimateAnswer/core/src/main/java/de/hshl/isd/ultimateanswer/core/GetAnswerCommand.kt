package de.hshl.isd.ultimateanswer.core

import de.hshl.isd.ultimateanswer.common.CommandHandler
import de.hshl.isd.ultimateanswer.core.ports.SuperComputer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GetAnswerCommand(val superComputer: SuperComputer) :
    CommandHandler<GetAnswerCommandDTO, String> {

    override fun execute(
        inDTO: GetAnswerCommandDTO,
        successHandler: (success: String) -> Unit,
        errorHandler: (error: Throwable) -> Unit
    ) {
        val scope = MainScope()
        scope.launch {
            val result = superComputer.answer(inDTO.question)
            withContext(Dispatchers.Main) {
                result.onSuccess { successHandler(it) }
                result.onFailure { errorHandler(it) }
            }
        }

    }

}