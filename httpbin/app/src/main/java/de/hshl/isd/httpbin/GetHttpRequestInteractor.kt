package de.hshl.isd.httpbin

import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Presenter
import de.hshl.isd.basiccleanarch.Response
import de.hshl.isd.basiccleanarch.UseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GetHttpRequestInteractor(
    override val presenter: Presenter<HttpRequestModel, String>,
    val gateway: HttpBinGateway
) :
    UseCase<Void?, HttpRequestModel, String> {

    override fun execute(
        request: Void?, displayer: Displayer<String>,
        requestCode: Int
    ) {
        val scope = MainScope()
        scope.launch {
            withContext(Dispatchers.IO) {

                when (val result = gateway.fetchData()) {
                    is Response.Success<*> -> {
                        val entity = result.value as HttpRequestModel
                        val viewModel = presenter.present(entity)
                        withContext(Dispatchers.Main) {
                            displayer.display(viewModel)
                        }
                    }
                    is Response.Failure -> {
                        withContext(Dispatchers.Main) {
                            displayer.display(result.error)
                        }
                    }
                }
            }
        }
    }
}

