package de.hshl.isd.listcompose

import androidx.compose.Composable
import androidx.compose.remember
import androidx.compose.state
import androidx.ui.core.Modifier
import androidx.ui.foundation.ScrollableColumn
import androidx.ui.foundation.TextField
import androidx.ui.input.TextFieldValue
import androidx.ui.layout.Column
import androidx.ui.layout.padding
import androidx.ui.unit.dp

@Composable
fun SearchList(state: SearchListState) {
    val rememberedState = remember { state }

    Column {
        val filterTextField = state { TextFieldValue("") }
        TextField(value = filterTextField.value,
            modifier = Modifier.padding(8.dp),
            onValueChange = {
                filterTextField.value = it
                rememberedState.filter = it.text
            })
        ScrollableColumn {
            rememberedState.filteredItems.forEach { item ->
                ItemRow(item)
            }
        }
    }
}
