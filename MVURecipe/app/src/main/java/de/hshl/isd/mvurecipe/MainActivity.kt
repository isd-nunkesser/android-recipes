package de.hshl.isd.mvurecipe

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.*
import androidx.ui.core.Modifier
import androidx.ui.core.setContent
import androidx.ui.foundation.Text
import androidx.ui.foundation.TextField
import androidx.ui.input.TextFieldValue
import androidx.ui.layout.Arrangement
import androidx.ui.layout.Column
import androidx.ui.layout.Row
import androidx.ui.layout.padding
import androidx.ui.material.Button
import androidx.ui.material.Scaffold
import androidx.ui.material.TopAppBar
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import de.hshl.isd.mvurecipe.ui.MVURecipeTheme

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MVURecipeTheme {
                MainContent()
            }
        }
    }
}

@Composable
fun MainContent() {
    val state = remember { MainState() }
    var name by stateFor(
            state.forename) { "${state.forename} ${state.surname}" }

    Scaffold(
            topBar = {
                TopAppBar(title = { Text(text = "MVU") })
            },
            bodyContent = {
                Column(verticalArrangement = Arrangement.Center) {
                    var forenameTextFieldValue by state {
                        TextFieldValue("Forename")
                    }
                    TextField(value = forenameTextFieldValue,
                            modifier = Modifier.padding(8.dp),
                            onValueChange = {
                                forenameTextFieldValue = it
                                state.forename = it.text
                            })
                    var surnameTextFieldValue by state {
                        TextFieldValue("Surname")
                    }
                    TextField(value = surnameTextFieldValue,
                            modifier = Modifier.padding(8.dp),
                            onValueChange = {
                                surnameTextFieldValue = it
                                state.surname = it.text
                            })
                    Row(modifier = Modifier.padding(8.dp)) {
                        Text(text = "Forename")
                        Text(
                                text = state.forename,
                                modifier = Modifier.padding(8.dp)
                        )
                    }
                    Row(modifier = Modifier.padding(8.dp)) {
                        Text(text = "Surname")
                        Text(
                                text = state.surname,
                                modifier = Modifier.padding(8.dp)
                        )
                    }
                    Row(modifier = Modifier.padding(8.dp)) {
                        Text(text = "Complete Name")
                        Text(
                                text = name,
                                modifier = Modifier.padding(8.dp)
                        )
                    }
                    Button(modifier = Modifier.padding(8.dp),
                            onClick = { state.reset() }) {
                        Text("Reset")
                    }
                }
            })
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MVURecipeTheme {
        MainContent()
    }
}
