/*
 * Copyright 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.hshl.isd.mockitodemo

import android.content.SharedPreferences
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Matchers
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.eq
import org.mockito.runners.MockitoJUnitRunner
import java.util.*


/**
 * Unit tests for the [SharedPreferencesHelper] that mocks [SharedPreferences].
 */
@RunWith(MockitoJUnitRunner::class)
class SharedPreferencesHelperTest {

    @Mock
    internal var mMockSharedPreferences: SharedPreferences? = null
    @Mock
    internal var mMockBrokenSharedPreferences: SharedPreferences? = null
    @Mock
    internal var mMockEditor: SharedPreferences.Editor? = null
    @Mock
    internal var mMockBrokenEditor: SharedPreferences.Editor? = null

    private var mSharedPreferenceEntry: SharedPreferenceEntry? = null
    private var mMockSharedPreferencesHelper: SharedPreferencesHelper? = null
    private var mMockBrokenSharedPreferencesHelper: SharedPreferencesHelper? = null

    companion object {

        private val TEST_NAME = "Test name"

        private val TEST_EMAIL = "test@email.com"

        private val TEST_DATE_OF_BIRTH = Calendar.getInstance()

        init {
            TEST_DATE_OF_BIRTH.set(1980, 1, 1)
        }
    }

    @Before
    fun initMocks() {
        // Create SharedPreferenceEntry to persist.
        mSharedPreferenceEntry = SharedPreferenceEntry(
            TEST_NAME, TEST_DATE_OF_BIRTH,
            TEST_EMAIL
        )

        // Create a mocked SharedPreferences.
        mMockSharedPreferencesHelper = createMockSharedPreference()

        // Create a mocked SharedPreferences that fails at saving data.
        mMockBrokenSharedPreferencesHelper = createBrokenMockSharedPreference()
    }

    @Test
    fun sharedPreferencesHelper_SaveAndReadPersonalInformation() {
        // Save the personal information to SharedPreferences
        val success = mMockSharedPreferencesHelper!!.savePersonalInfo(mSharedPreferenceEntry!!)

        assertThat(
            "Checking that SharedPreferenceEntry.save... returns true",
            success, `is`(true)
        )

        // Read personal information from SharedPreferences
        val savedSharedPreferenceEntry = mMockSharedPreferencesHelper!!.personalInfo

        // Make sure both written and retrieved personal information are equal.
        assertThat(
            "Checking that SharedPreferenceEntry.name has been persisted and read correctly",
            mSharedPreferenceEntry!!.name,
            `is`(equalTo(savedSharedPreferenceEntry.name))
        )
        assertThat(
            "Checking that SharedPreferenceEntry.dateOfBirth has been persisted and read " + "correctly",
            mSharedPreferenceEntry!!.dateOfBirth,
            `is`(equalTo(savedSharedPreferenceEntry.dateOfBirth))
        )
        assertThat(
            "Checking that SharedPreferenceEntry.email has been persisted and read " + "correctly",
            mSharedPreferenceEntry!!.email,
            `is`(equalTo(savedSharedPreferenceEntry.email))
        )
    }

    @Test
    fun sharedPreferencesHelper_SavePersonalInformationFailed_ReturnsFalse() {
        // Read personal information from a broken SharedPreferencesHelper
        val success = mMockBrokenSharedPreferencesHelper!!.savePersonalInfo(mSharedPreferenceEntry!!)
        assertThat(
            "Makes sure writing to a broken SharedPreferencesHelper returns false", success,
            `is`(false)
        )
    }

    /**
     * Creates a mocked SharedPreferences.
     */
    private fun createMockSharedPreference(): SharedPreferencesHelper {
        // Mocking reading the SharedPreferences as if mMockSharedPreferences was previously written
        // correctly.
        `when`(mMockSharedPreferences!!.getString(eq(SharedPreferencesHelper.KEY_NAME), Matchers.anyString()))
            .thenReturn(mSharedPreferenceEntry!!.name)
        `when`(mMockSharedPreferences!!.getString(eq(SharedPreferencesHelper.KEY_EMAIL), Matchers.anyString()))
            .thenReturn(mSharedPreferenceEntry!!.email)
        `when`(mMockSharedPreferences!!.getLong(eq(SharedPreferencesHelper.KEY_DOB), Matchers.anyLong()))
            .thenReturn(mSharedPreferenceEntry!!.dateOfBirth.timeInMillis)

        // Mocking a successful commit.
        `when`(mMockEditor!!.commit()).thenReturn(true)

        // Return the MockEditor when requesting it.
        `when`<SharedPreferences.Editor>(mMockSharedPreferences!!.edit()).thenReturn(mMockEditor)
        return SharedPreferencesHelper(mMockSharedPreferences!!)
    }

    /**
     * Creates a mocked SharedPreferences that fails when writing.
     */
    private fun createBrokenMockSharedPreference(): SharedPreferencesHelper {
        // Mocking a commit that fails.
        `when`(mMockBrokenEditor!!.commit()).thenReturn(false)

        // Return the broken MockEditor when requesting it.
        `when`<SharedPreferences.Editor>(mMockBrokenSharedPreferences!!.edit()).thenReturn(mMockBrokenEditor)
        return SharedPreferencesHelper(mMockBrokenSharedPreferences!!)
    }

}