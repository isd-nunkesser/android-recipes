package de.hshl.isd.mlkitsmartreply

import com.google.firebase.ml.naturallanguage.smartreply.SmartReplySuggestion
import de.hshl.isd.basiccleanarch.Presenter

class SmartReplySuggestionsPresenter : Presenter<List<SmartReplySuggestion>, List<String>> {
    override fun present(model: List<SmartReplySuggestion>): List<String> {
        return model.map { it.text }
    }
}