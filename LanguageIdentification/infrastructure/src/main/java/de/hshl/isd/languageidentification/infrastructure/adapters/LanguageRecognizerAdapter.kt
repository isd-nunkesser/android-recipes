package de.hshl.isd.languageidentification.infrastructure.adapters

import com.google.mlkit.nl.languageid.LanguageIdentification
import de.hshl.isd.languageidentification.core.ports.LanguageRecognizer
import java.lang.Exception

class LanguageRecognizerAdapter : LanguageRecognizer {

    private val languageIdentifier = LanguageIdentification.getClient()

    override fun identifyLanguage(text: String,
                                  successHandler: (success: String) -> Unit,
                                  errorHandler: (error: Throwable) -> Unit) {
        languageIdentifier.identifyLanguage(text)
                .addOnSuccessListener { languageCode ->
                    if (languageCode == "und") {
                        errorHandler(Exception("Can't identify language."))
                    } else {
                        successHandler(languageCode)
                    }
                }
                .addOnFailureListener {
                    errorHandler(it)
                }
    }

    override fun identifyPossibleLanguages(text: String,
                                           successHandler: (success: List<Pair<String,Float>>) -> Unit,
                                           errorHandler: (error: Throwable) -> Unit) {
        languageIdentifier.identifyPossibleLanguages(text)
                .addOnSuccessListener { identifiedLanguages ->
                    successHandler(identifiedLanguages.map { Pair(it.languageTag, it.confidence) })
                }
                .addOnFailureListener {
                    errorHandler(it)
                }
    }
}