package de.hshl.isd.mlkitlanguageidentification

import de.hshl.isd.basiccleanarch.Presenter

class IdentifiedLanguagePresenter : Presenter<String, String> {
    override fun present(model: String): String {
        return "Identified Language: ${model}"
    }
}