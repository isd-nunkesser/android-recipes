package de.hshl.isd.mlkitlanguageidentification

data class TextRequest(val text: String)
