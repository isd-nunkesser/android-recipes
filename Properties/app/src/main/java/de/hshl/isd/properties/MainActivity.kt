package de.hshl.isd.properties

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val props = Properties()
        try {
            props.load(this.assets.open("config.properties"))
            Log.i("MainActivity", props.getProperty("url"))
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}
