package de.hshl.isd.viewmodelrecipe

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {
    private val _data = MutableLiveData<Int>()
    private val data: LiveData<Int>
        get() = _data

    init {
        _data.value = 0
    }

    val label : LiveData<String> =
        Transformations.map(data) { it.toString()}

    fun increase() {
        _data.value = _data.value!! + 1
    }
}
