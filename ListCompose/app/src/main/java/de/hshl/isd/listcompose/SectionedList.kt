package de.hshl.isd.listcompose

import androidx.compose.Composable
import androidx.ui.foundation.ScrollableColumn

@Composable
fun SectionedList(sections: Map<String, List<ItemViewModel>>) {
    ScrollableColumn {
        sections.forEach { (title, items) ->
            SectionHeader(title = title)
            items.forEach { item ->
                ItemRow(item)
            }
        }
    }
}