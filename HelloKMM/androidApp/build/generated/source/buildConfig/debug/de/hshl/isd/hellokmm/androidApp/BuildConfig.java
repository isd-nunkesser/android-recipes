/**
 * Automatically generated file. DO NOT MODIFY
 */
package de.hshl.isd.hellokmm.androidApp;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "de.hshl.isd.hellokmm.androidApp";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
