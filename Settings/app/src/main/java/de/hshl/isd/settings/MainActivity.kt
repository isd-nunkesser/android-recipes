package de.hshl.isd.settings

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import de.hshl.isd.settings.ui.main.SettingsFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, SettingsFragment())
                    .commitNow()
        }
    }

}
