package de.hshl.isd.asynccompose

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.getValue
import androidx.compose.setValue
import androidx.compose.state
import androidx.ui.core.setContent
import androidx.ui.foundation.Text
import androidx.ui.layout.Column
import androidx.ui.material.Button
import androidx.ui.material.MaterialTheme
import androidx.ui.tooling.preview.Preview

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                MainContent()
            }
        }
    }

}

@Composable
fun MainContent() {
    var resultText by state { "" }
    val asyncExample = AsyncExample()

    Column {
        Button(onClick = {
            asyncExample.backgroundExample()
        }) {
            Text("Background example")
        }
        Button(onClick = {
            asyncExample.uiExample {
                resultText = it
            }
        }) {
            Text("UI example")
        }
        Text(resultText)
    }

}

@Preview
@Composable
fun DefaultPreview() {
    MaterialTheme {
        MainContent()
    }
}
