package de.hshl.isd.navigationcompose

import androidx.compose.getValue
import androidx.compose.mutableStateOf
import androidx.compose.setValue

sealed class Screen {
    object Home : Screen()
    object Dashboard : Screen()
    data class SecondLevel(val message: String) : Screen()
}

object Status {
    var currentScreen by mutableStateOf<Screen>(Screen.Home)
}
