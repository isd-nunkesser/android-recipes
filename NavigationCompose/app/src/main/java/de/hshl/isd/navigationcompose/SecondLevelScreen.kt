package de.hshl.isd.navigationcompose

import androidx.compose.Composable
import androidx.ui.foundation.Icon
import androidx.ui.foundation.Text
import androidx.ui.material.IconButton
import androidx.ui.material.Scaffold
import androidx.ui.material.TopAppBar
import androidx.ui.material.icons.Icons
import androidx.ui.material.icons.filled.ArrowBack

@Composable
fun SecondLevelScreen(message: String) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = "Second Level Screen")
                },
                navigationIcon = {
                    IconButton(onClick = {
                        Status.currentScreen = Screen.Home
                    }) {
                        Icon(Icons.Filled.ArrowBack)
                    }
                }
            )
        },
        bodyContent = {
            Text(message)
        }
    )
}
