package de.hshl.isd.eventcalendar

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.ui.core.setContent
import androidx.ui.foundation.Text
import androidx.ui.material.Button
import androidx.ui.material.MaterialTheme
import androidx.ui.tooling.preview.Preview
import de.hshl.isd.eventcalendar.ui.EventCalendarTheme

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme()
            {
                Button(onClick = {
                    val intent = Intent(this, EventCalendar::class.java)
                    startActivity(intent)
                }) {
                    Text("Event Calendar")
                }
            }
        }
    }
}

