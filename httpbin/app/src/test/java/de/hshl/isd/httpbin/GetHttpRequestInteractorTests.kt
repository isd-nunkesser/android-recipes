package de.hshl.isd.httpbin

import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Response
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import java.util.concurrent.CompletableFuture

class GetHttpRequestInteractorTests {
    @Mock
    private var mockPresenter = mock(HttpRequestPresenter::class.java)

    @Mock
    private var mockGateway = mock(HttpBinGateway::class.java)

    private class TestDisplayer(val future: CompletableFuture<Int>) :
        Displayer<String> {
        override fun display(success: String, requestCode: Int) {
            future.complete(1)
        }

        override fun display(error: Throwable) {
            future.complete(1)
        }

    }

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun testCallDisplayer() {
        val future = CompletableFuture<Int>()

        val model = HttpRequestModel("", "")
        `when`(mockGateway.fetchData()).thenReturn(
            Response.Success<HttpRequestModel>(model)
        )

        `when`(mockPresenter.present(model)).thenReturn("")

        GetHttpRequestInteractor(mockPresenter, mockGateway).execute(
            null,
            TestDisplayer(future)
        )
        assertEquals(1, future.get() as Int)
    }
}