package de.hshl.isd.list

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import de.hshl.isd.list.ui.main.ItemFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, ItemFragment())
                    .commitNow()
        }
    }

}
