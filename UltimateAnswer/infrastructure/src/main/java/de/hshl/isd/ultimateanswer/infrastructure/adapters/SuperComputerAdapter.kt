package de.hshl.isd.ultimateanswer.infrastructure.adapters

import de.hshl.isd.ultimateanswer.core.ports.SuperComputer
import de.hshl.isd.ultimateanswer.infrastructure.DeepThought

class SuperComputerAdapter(var adaptee: DeepThought = DeepThought()) :
    SuperComputer {

    override suspend fun answer(question: String): Result<String> {
        return adaptee.provideAnswer().map { it.toString() }
    }
}