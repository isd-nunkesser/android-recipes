package de.hshl.isd.placeholderposts.infrastructure.adapters

import de.hshl.isd.placeholderposts.common.Repository
import de.hshl.isd.placeholderposts.core.PostEntity
import de.hshl.isd.placeholderposts.infrastructure.JSONPlaceholderAPI
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PostRepositoryAdapter : Repository<Int, PostEntity> {

    val retrofit =
        Retrofit.Builder().baseUrl("https://jsonplaceholder.typicode.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    var service: JSONPlaceholderAPI

    init {
        service = retrofit.create(JSONPlaceholderAPI::class.java)
    }

    override suspend fun create(entity: PostEntity): Result<Long> {
        return try {
            val response = service.createPost(entity.toPost()).execute()
            Result.success(response.body()!!.id)
        } catch (t: Throwable) {
            Result.failure(t)
        }
    }

    override suspend fun retrieve(id: Int): Result<PostEntity> {
        return try {
            val response = service.readPost(id).execute()
            Result.success(response.body()!!.toPostEntity())
        } catch (t: Throwable) {
            Result.failure(t)
        }
    }

    override suspend fun retrieveAll(): Result<List<PostEntity>> {
        return try {
            val response = service.readAllPosts().execute()
            Result.success(response.body()!!.map { it.toPostEntity() })
        } catch (t: Throwable) {
            Result.failure(t)
        }
    }

    override suspend fun update(id: Int, entity: PostEntity): Result<Boolean> {
        TODO("Not yet implemented")
    }

    override suspend fun delete(id: Int): Result<Boolean> {
        TODO("Not yet implemented")
    }
}