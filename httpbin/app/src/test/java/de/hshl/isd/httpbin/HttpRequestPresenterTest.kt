package de.hshl.isd.httpbin

import org.junit.Assert.assertEquals
import org.junit.Test

class HttpRequestPresenterTest {

    @Test
    fun testPresent() {
        val model = HttpRequestModel(origin = "origin", url = "url")
        val viewModel = HttpRequestPresenter().present(model)
        assertEquals("Origin: origin, Url: url", viewModel)
    }

}
