package de.hshl.isd.ultimateanswer

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.getValue
import androidx.compose.setValue
import androidx.compose.state
import androidx.ui.core.setContent
import androidx.ui.foundation.Text
import androidx.ui.foundation.TextField
import androidx.ui.input.TextFieldValue
import androidx.ui.layout.Column
import androidx.ui.material.Button
import androidx.ui.material.MaterialTheme
import androidx.ui.tooling.preview.Preview
import de.hshl.isd.ultimateanswer.core.GetAnswerCommand
import de.hshl.isd.ultimateanswer.core.GetAnswerCommandDTO
import de.hshl.isd.ultimateanswer.infrastructure.adapters.SuperComputerAdapter

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                MainContent()
            }
        }
    }
}

@Composable
fun MainContent() {
    val tag = "MainContent"
    var question by state { TextFieldValue("Question") }
    var resultText by state { "" }
    var service = GetAnswerCommand(SuperComputerAdapter())


    fun success(value: String) {
        resultText = value
    }

    fun failure(error: Throwable) {
        Log.e(tag, error.localizedMessage!!)
    }

    Column {
        TextField(
            value = question,
            onValueChange = {
                question = it
            }
        )
        Button(onClick = {
            service.execute(
                GetAnswerCommandDTO(question.text),
                ::success,
                ::failure
            )
        }) {
            Text("Start")
        }
        Text(resultText)
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MaterialTheme {
        MainContent()
    }
}