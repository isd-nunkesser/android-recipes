package de.hshl.isd.mockitodemo

import android.content.Context

class ClassUnderTest(private val context: Context) {
    fun getHelloWorldString(): String = context.getString(R.string.hello_world)
}
