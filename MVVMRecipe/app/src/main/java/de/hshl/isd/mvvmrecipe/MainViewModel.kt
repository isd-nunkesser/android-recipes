package de.hshl.isd.mvvmrecipe

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    // Two way bindable values
    val forename = MutableLiveData<String>()
    val surname = MutableLiveData<String>()

    // One way bindable value
    private val _name = MutableLiveData<String>()
    val name: LiveData<String>
        get() = _name

    init {
        _name.value = ""
    }

    // Mediated value
    val mediatorName: MediatorLiveData<String> by lazy {
        MediatorLiveData<String>()
    }

    init {
        mediatorName.addSource(forename,
            { mediatorName.value = """${forename.value} ${surname.value}""" })
        mediatorName.addSource(surname,
            { mediatorName.value = """${forename.value} ${surname.value}""" })
    }

    fun computeName() {
        _name.value = """${forename.value} ${surname.value}"""
    }

}
