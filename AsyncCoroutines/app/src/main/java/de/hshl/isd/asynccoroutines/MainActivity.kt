package de.hshl.isd.asynccoroutines

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val tag = "MainActivity"
    val asyncExample = AsyncExample()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        backgroundButton.setOnClickListener { asyncExample.backgroundExample() }
        uiButton.setOnClickListener {
            asyncExample.uiExample {
                resultText.text = it
            }
        }
    }

}
