package de.hshl.isd.languageidentification.core.ports

interface LanguageRecognizer {

    fun identifyLanguage(text: String, successHandler: (success: String) -> Unit,
                         errorHandler: (error: Throwable) -> Unit)

    fun identifyPossibleLanguages(text: String, successHandler: (success: List<Pair<String,Float>>) -> Unit,
                         errorHandler: (error: Throwable) -> Unit)

}