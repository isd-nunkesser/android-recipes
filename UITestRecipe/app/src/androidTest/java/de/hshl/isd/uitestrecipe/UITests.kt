package de.hshl.isd.uitestrecipe

import androidx.compose.Composable
import androidx.test.filters.MediumTest
import androidx.ui.material.MaterialTheme
import androidx.ui.material.Surface
import androidx.ui.test.ComposeTestRule
import androidx.ui.test.assertIsDisplayed
import androidx.ui.test.createComposeRule
import androidx.ui.test.onNodeWithText
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

fun ComposeTestRule.setMaterialContent(children: @Composable() () -> Unit) {
    setContent {
        MaterialTheme {
            Surface {
                children()
            }
        }
    }
}


@MediumTest
@RunWith(JUnit4::class)
class UITests {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setUp() {
        composeTestRule.setMaterialContent { Greeting(name = "Android") }
    }

    @Test
    fun greetingIsDisplayed() {
        onNodeWithText("Hello Android!").assertIsDisplayed()
    }
}