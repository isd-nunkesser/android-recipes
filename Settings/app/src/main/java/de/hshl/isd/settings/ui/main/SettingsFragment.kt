package de.hshl.isd.settings.ui.main


import android.os.Bundle
import android.support.v7.preference.PreferenceFragmentCompat
import de.hshl.isd.settings.R

class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?,
                                     rootKey: String?) {
        setPreferencesFromResource(R.xml.pref_general, rootKey)
    }
}
