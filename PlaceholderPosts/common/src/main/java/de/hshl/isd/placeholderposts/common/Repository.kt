package de.hshl.isd.placeholderposts.common

interface Repository<IdType, EntityType> {
    suspend fun create(entity: EntityType): Result<Long>
    suspend fun retrieve(id: IdType): Result<EntityType>
    suspend fun retrieveAll(): Result<List<EntityType>>
    suspend fun update(id: IdType, entity: EntityType): Result<Boolean>
    suspend fun delete(id: IdType): Result<Boolean>
}