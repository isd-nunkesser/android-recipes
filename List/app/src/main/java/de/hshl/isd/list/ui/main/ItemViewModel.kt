package de.hshl.isd.list.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ItemViewModel : ViewModel() {

    private val _data = MutableLiveData<List<String>>()

    val data: LiveData<List<String>>
        get() = _data

    init {
        _data.value = listOf("Teddy bear", "Banana", "Sponge", "Laptop")
    }

}
