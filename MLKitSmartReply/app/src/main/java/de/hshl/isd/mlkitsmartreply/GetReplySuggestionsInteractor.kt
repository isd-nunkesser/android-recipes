package de.hshl.isd.mlkitsmartreply

import com.google.firebase.ml.naturallanguage.smartreply.FirebaseTextMessage
import com.google.firebase.ml.naturallanguage.smartreply.SmartReplySuggestion
import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Presenter
import de.hshl.isd.basiccleanarch.Response
import de.hshl.isd.basiccleanarch.UseCase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class GetReplySuggestionsInteractor(
    override val presenter: Presenter<List<SmartReplySuggestion>, List<String>>,
    val gateway: MLKitGateway
) : UseCase<List<FirebaseTextMessage>, List<SmartReplySuggestion>, List<String>> {

    override fun execute(
        request: List<FirebaseTextMessage>,
        displayer: Displayer<List<String>>,
        requestCode: Int
    ) {
        GlobalScope.async {
            val result = MLKitGateway().suggestReplies(request)
            when (result) {
                is Response.Success<*> -> {
                    val suggestions = result.value as List<SmartReplySuggestion>
                    displayer.display(presenter.present(suggestions))
                }
                is Response.Failure -> {
                    displayer.display(result.error)
                }
            }
        }
    }


}