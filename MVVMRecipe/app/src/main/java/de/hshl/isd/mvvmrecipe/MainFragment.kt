package de.hshl.isd.mvvmrecipe

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() =
            MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        // One way bindings for forenameTextView, surnameEditText,
        // surnameTextView, nameTextView, and computedNameTextView
        viewModel.forename.observe(this, Observer<String> {
            forenameTextView.text = it
        })

        viewModel.surname.observe(this, Observer<String> { surname ->
            surnameEditText.takeUnless { surname == it.text.toString() }
                ?.setText(surname)
            surnameTextView.text = surname
        })

        viewModel.mediatorName.observe(this, Observer<String> {
            nameTextView.text = it
        })

        viewModel.name.observe(this, Observer<String> {
            computedNameTextView.text = it
        })

        // One way to source binding for forenameEditText and surnameEditText
        forenameEditText.addTextChangedListener(
            SourceBindingTextWatcher(
                viewModel.forename
            )
        )

        surnameEditText.addTextChangedListener(
            SourceBindingTextWatcher(
                viewModel.surname
            )
        )

        resetButton.setOnClickListener {
            viewModel.forename.value = ""
            viewModel.surname.value = ""
        }

        computeButton.setOnClickListener {
            viewModel.computeName()
        }

    }

}
