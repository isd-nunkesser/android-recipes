package de.hshl.isd.listcompose

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.state
import androidx.ui.core.Modifier
import androidx.ui.core.setContent
import androidx.ui.foundation.Box
import androidx.ui.foundation.Text
import androidx.ui.layout.Column
import androidx.ui.material.MaterialTheme
import androidx.ui.material.Tab
import androidx.ui.material.TabRow
import androidx.ui.material.TopAppBar
import androidx.ui.tooling.preview.Preview

private enum class Sections(val title: String) {
    StaticList("Statisch"),
    SimpleList("Simple"),
    SectionedList("Sections"),
    SearchList("Search")
}

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                MainContent()
            }
        }
    }
}

@Composable
fun MainContent() {
    val dummyItems = listOf(
        ItemViewModel("Teddy bear", "Subtitle 1"),
        ItemViewModel("Banana", "Subtitle 2"),
        ItemViewModel("Sponge", "Subtitle 3"),
        ItemViewModel("Laptop", "Subtitle 4")
    )

    val dummySections = mapOf(
        "Section 1" to dummyItems,
        "Section 2" to listOf<ItemViewModel>(ItemViewModel("Lorem Ipsum in Section 2", "Some sub"))
    )

    val (currentSection, updateSection) = state { Sections.SearchList }
    val sectionTitles = Sections.values().map { it.title }

    Column {
        TopAppBar(
            title = { Text("Interests") }
        )
        TabRow(items = sectionTitles, selectedIndex = currentSection.ordinal) { index, text ->
            Tab(text = { Text(text) }, selected = currentSection.ordinal == index, onSelected = {
                updateSection(Sections.values()[index])
            })
        }
        Box(modifier = Modifier.weight(1f)) {
            when (currentSection) {
                Sections.StaticList -> StaticList()
                Sections.SimpleList -> SimpleList(items = dummyItems)
                Sections.SectionedList -> SectionedList(sections = dummySections)
                Sections.SearchList -> SearchList(state = SearchListState(dummyItems))
            }
        }
    }
}


@Preview
@Composable
fun DefaultPreview() {
    MaterialTheme {
        MainContent()
    }
}