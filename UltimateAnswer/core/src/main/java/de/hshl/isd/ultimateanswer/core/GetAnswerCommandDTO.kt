package de.hshl.isd.ultimateanswer.core

data class GetAnswerCommandDTO(val question: String)
