package de.hshl.isd.basicinteractionxml.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import de.hshl.isd.basicinteractionxml.R

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var button: Button
    private lateinit var editTextTextPersonName: EditText
    private lateinit var message: TextView
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        editTextTextPersonName = requireActivity().findViewById(R.id.editTextTextPersonName)
        button = requireActivity().findViewById(R.id.button)
        message = requireActivity().findViewById(R.id.message)

        button.setOnClickListener { message.text = editTextTextPersonName.text.toString().toUpperCase() }

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel


    }

}