package de.hshl.isd.placeholderposts.infrastructure

data class Post(
    val userId: Long,
    val id: Long,
    val title: String,
    val body: String
)
