package de.hshl.isd.mlkitcustommodel

import com.google.firebase.ml.common.modeldownload.FirebaseLocalModel
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager
import com.google.firebase.ml.custom.FirebaseModelInterpreter
import com.google.firebase.ml.custom.FirebaseModelOptions

class MLKitGateway {

    fun loadAndRegisterLocalModel(name: String): FirebaseModelInterpreter? {
        val localSource = FirebaseLocalModel.Builder(name)
            .setAssetFilePath("${name}.tflite")
            .build()
        FirebaseModelManager.getInstance().registerLocalModel(localSource)

        val options = FirebaseModelOptions.Builder()
            .setLocalModelName(name)
            .build()
        return FirebaseModelInterpreter.getInstance(options)
    }
}