package de.hshl.isd.minimalcleanarchcompose

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.getValue
import androidx.compose.setValue
import androidx.compose.state
import androidx.ui.core.setContent
import androidx.ui.foundation.Text
import androidx.ui.layout.Column
import androidx.ui.material.Button
import androidx.ui.material.MaterialTheme
import androidx.ui.tooling.preview.Preview
import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.minimalcleanarch.MinimalInteractor
import de.hshl.isd.minimalcleanarch.MinimalPresenter

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                MainContent()
            }
        }
    }
}

@Composable
fun MainContent() {
    val tag = "MainContent"
    var resultText by state { "" }
    var interactor = MinimalInteractor(MinimalPresenter())
    val displayer = object : Displayer<String> {
        override fun display(success: String, requestCode: Int) {
            resultText = success
        }

        override fun display(error: Throwable) {
            Log.e(tag, error.localizedMessage)
        }
    }

    Column {
        Button(onClick = { interactor.execute(null, displayer) }) {
            Text("Start")
        }
        Text(resultText)
    }
}

@Preview
@Composable
fun DefaultPreview() {
    MaterialTheme {
        MainContent()
    }
}