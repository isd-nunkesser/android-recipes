package de.hshl.isd.mlkitsmartreply

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.google.firebase.ml.naturallanguage.smartreply.FirebaseTextMessage
import de.hshl.isd.basiccleanarch.Displayer
import java.util.*

class ChatViewModel : ViewModel(), Displayer<List<String>> {
    private val REMOTE_USER_ID = UUID.randomUUID().toString()

    private val suggestions = MediatorLiveData<List<String>>()
    private val messageList = MutableLiveData<MutableList<Message>>()
    private val emulatingRemoteUser = MutableLiveData<Boolean>()

    val messages: LiveData<MutableList<Message>>
        get() = messageList

    init {
        initSuggestionsGenerator()
        emulatingRemoteUser.postValue(false)
    }

    fun getSuggestions(): LiveData<List<String>> {
        return suggestions
    }

    fun getEmulatingRemoteUser(): LiveData<Boolean> {
        return emulatingRemoteUser
    }

    internal fun setMessages(messages: MutableList<Message>) {
        clearSuggestions()
        messageList.postValue(messages)
    }

    internal fun switchUser() {
        clearSuggestions()
        val value = emulatingRemoteUser.value!!
        emulatingRemoteUser.postValue(!value)
    }

    private fun clearSuggestions() {
        suggestions.postValue(ArrayList())
    }

    internal fun addMessage(message: String) {
        var list: MutableList<Message>? = messageList.value
        if (list == null) {
            list = ArrayList()
        }
        val value = emulatingRemoteUser.value!!
        list.add(Message(message, !value, System.currentTimeMillis()))
        clearSuggestions()
        messageList.postValue(list)
    }

    private fun initSuggestionsGenerator() {
        suggestions.addSource(emulatingRemoteUser, Observer { isEmulatingRemoteUser ->
            val list = messageList.value
            if (list == null || list.isEmpty()) {
                return@Observer
            }

            generateReplies(list, isEmulatingRemoteUser!!)
        })

        suggestions.addSource(messageList, Observer { list ->
            val isEmulatingRemoteUser = emulatingRemoteUser.value
            if (isEmulatingRemoteUser == null || list!!.isEmpty()) {
                return@Observer
            }

            generateReplies(list, isEmulatingRemoteUser)
        })
    }

    private fun generateReplies(
        messages: List<Message>,
        isEmulatingRemoteUser: Boolean
    ) {
        val lastMessage = messages[messages.size - 1]

        // If the last message in the chat thread is not sent by the "other" user, don't generate
        // smart replies.
        if (lastMessage.isLocalUser && !isEmulatingRemoteUser || !lastMessage.isLocalUser && isEmulatingRemoteUser) {
            Log.e("ChatViewModel", "Not running smart reply!")
            return
        }

        val chatHistory = ArrayList<FirebaseTextMessage>()
        for (message in messages) {
            if (message.isLocalUser && !isEmulatingRemoteUser || !message.isLocalUser && isEmulatingRemoteUser) {
                chatHistory.add(
                    FirebaseTextMessage.createForLocalUser(
                        message.text,
                        message.timestamp
                    )
                )
            } else {
                chatHistory.add(
                    FirebaseTextMessage.createForRemoteUser(
                        message.text,
                        message.timestamp, REMOTE_USER_ID
                    )
                )
            }
        }

        GetReplySuggestionsInteractor(SmartReplySuggestionsPresenter(), MLKitGateway()).execute(
            chatHistory, this
        )

    }

    override fun display(success: List<String>, requestCode: Int) {
        suggestions.postValue(success)
    }

    override fun display(error: Throwable) {
        Log.e("ChatViewModel", error.toString())
    }


}
