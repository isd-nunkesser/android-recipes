package de.hshl.isd.eventcalendar

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.skyhope.eventcalenderlibrary.model.Event
import java.util.*

class EventCalendar : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_calendar)

        val calendarEvent = findViewById<com.skyhope.eventcalenderlibrary.CalenderEvent>(R.id.calendarEvent)
        val calendar = GregorianCalendar()
        val event = Event(calendar.timeInMillis, "Test", Color.RED)
        calendarEvent.addEvent(event)
    }
}