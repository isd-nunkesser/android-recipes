package de.hshl.isd.ultimateanswer.infrastructure

import kotlinx.coroutines.delay

class DeepThought {

    suspend fun provideAnswer(): Result<Int> {
        delay(1000L)
        return Result.success(42)
    }
}