package de.hshl.isd.minimalcleanarch

import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Presenter
import de.hshl.isd.basiccleanarch.Response
import de.hshl.isd.basiccleanarch.UseCase

class MinimalInteractor(override val presenter: Presenter<Int, String>) :
    UseCase<Void?, Int, String> {
    override fun execute(request: Void?, displayer: Displayer<String>, requestCode: Int) {
        var result: Response = Response.Success<Int>(42)
        //var result : Response = Response.Failure(Exception("Simulate failure"))
        when (result) {
            is Response.Success<*> -> {
                val entity = result.value as Int
                val viewModel = presenter.present(entity)
                displayer.display(viewModel)
            }
            is Response.Failure -> {
                displayer.display(result.error)
            }
        }
    }


}