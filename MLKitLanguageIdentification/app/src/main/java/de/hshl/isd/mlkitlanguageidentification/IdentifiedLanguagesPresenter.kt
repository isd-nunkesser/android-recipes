package de.hshl.isd.mlkitlanguageidentification

import android.text.TextUtils
import com.google.firebase.ml.naturallanguage.languageid.IdentifiedLanguage
import de.hshl.isd.basiccleanarch.Presenter
import java.util.*

class IdentifiedLanguagesPresenter : Presenter<List<IdentifiedLanguage>, String> {
    override fun present(model: List<IdentifiedLanguage>): String {
        val detectedLanguages = model.map {
            String.format(
                Locale.US,
                "%s (%3f)",
                it.languageCode,
                it.confidence
            )
        }
        return TextUtils.join(", ", detectedLanguages)
    }

}