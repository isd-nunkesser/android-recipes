package de.hshl.isd.asynccoroutines

import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class AsyncUnitTest {

    private var testClass: AsyncExample? = null

    @Before
    fun setUp() {
        testClass = AsyncExample()
    }

    @Test
    fun test() {
        assertEquals(runBlocking { testClass!!.doSomethingUsefulOne() }, 13)
        assertEquals(runBlocking { testClass!!.doSomethingUsefulTwo() }, 29)
    }
}
