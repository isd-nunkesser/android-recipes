package de.hshl.isd.httpbin

import de.hshl.isd.basiccleanarch.Response
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.fail
import kotlinx.coroutines.runBlocking
import org.junit.Test

class HttpBinGatewayTest {

    @Test
    fun testFetchData() {
        runBlocking {
            when (val result = HttpBinGateway().fetchData()) {
                is Response.Success<*> -> {
                    val model = result.value as HttpRequestModel
                    assertNotNull(model.origin)
                    assertNotNull(model.url)
                }
                is Response.Failure -> {
                    fail(result.error.localizedMessage)
                }
            }
        }
    }
}