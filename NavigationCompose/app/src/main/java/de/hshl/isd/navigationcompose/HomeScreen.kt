package de.hshl.isd.navigationcompose

import androidx.compose.Composable
import androidx.compose.remember
import androidx.ui.foundation.Text
import androidx.ui.material.Button
import androidx.ui.material.DrawerState
import androidx.ui.material.Scaffold
import androidx.ui.material.ScaffoldState

@Composable
fun HomeScreen(scaffoldState: ScaffoldState = remember { ScaffoldState() }) {
    Scaffold(
        scaffoldState = scaffoldState,
        drawerContent = {
            AppDrawer(
                currentScreen = Screen.Home,
                closeDrawer = {
                    scaffoldState.drawerState = DrawerState.Closed
                }
            )
        },
        topBar = {
            DrawerTopAppBar(
                title = "Home Screen",
                scaffoldState = scaffoldState
            )
        },
        bodyContent = {
            Button(onClick = {
                Status.currentScreen =
                    Screen.SecondLevel("Hello second level")
            }) {
                Text("Navigate to second level")
            }
        }
    )
}
