package de.hshl.isd.navigationcompose

import androidx.compose.Composable
import androidx.ui.core.Modifier
import androidx.ui.layout.Column
import androidx.ui.layout.fillMaxSize
import androidx.ui.material.icons.Icons
import androidx.ui.material.icons.filled.Home
import androidx.ui.material.icons.filled.List

@Composable
fun AppDrawer(
    currentScreen: Screen,
    closeDrawer: () -> Unit
) {
    Column(modifier = Modifier.fillMaxSize()) {
        DrawerButton(
            icon = Icons.Filled.Home,
            label = "Home",
            isSelected = currentScreen == Screen.Home,
            action = {
                Status.currentScreen = Screen.Home
                closeDrawer()
            })

        DrawerButton(
            icon = Icons.Filled.List,
            label = "Dashboard",
            isSelected = currentScreen == Screen.Dashboard,
            action = {
                Status.currentScreen = Screen.Dashboard
                closeDrawer()
            })
    }
}