package de.hshl.isd.mlkitlanguageidentification

import com.google.firebase.ml.naturallanguage.languageid.IdentifiedLanguage
import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Presenter
import de.hshl.isd.basiccleanarch.Response
import de.hshl.isd.basiccleanarch.UseCase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class IdentifyPossibleLanguagesInteractor(
    override val presenter: Presenter<List<IdentifiedLanguage>, String>,
    val gateway: MLKitGateway
) : UseCase<TextRequest, List<IdentifiedLanguage>, String> {

    override fun execute(request: TextRequest, displayer: Displayer<String>, requestCode: Int) {
        GlobalScope.async {
            val result = gateway.identifyPossibleLanguages(request.text)
            when (result) {
                is Response.Success<*> -> {
                    val language = result.value as List<IdentifiedLanguage>
                    displayer.display(presenter.present(language))
                }
                is Response.Failure -> {
                    displayer.display(result.error)
                }
            }
        }

    }
}