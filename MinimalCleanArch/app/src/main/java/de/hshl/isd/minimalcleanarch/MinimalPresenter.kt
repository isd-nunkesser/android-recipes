package de.hshl.isd.minimalcleanarch

import de.hshl.isd.basiccleanarch.Presenter

class MinimalPresenter : Presenter<Int, String> {
    override fun present(model: Int): String {
        return model.toString()
    }
}