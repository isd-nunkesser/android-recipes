package de.hshl.isd.calendarevent

import android.content.Intent
import android.os.Bundle
import android.provider.CalendarContract
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addEventButton.setOnClickListener { addBlankEvent() }
        addPrefilledEventButton.setOnClickListener { addPrefilledEvent() }
    }

    private fun addBlankEvent() {
        val calIntent = Intent(Intent.ACTION_INSERT)
        calIntent.type = "vnd.android.cursor.item/event"
        startActivity(calIntent)

    }

    private fun addPrefilledEvent() {
        val calIntent = Intent(Intent.ACTION_INSERT)
        calIntent.type = "vnd.android.cursor.item/event"
        calIntent.putExtra(CalendarContract.Events.TITLE, "Event title")
        calIntent.putExtra(CalendarContract.Events.EVENT_LOCATION, "Event location")
        calIntent.putExtra(CalendarContract.Events.DESCRIPTION, "Event description")

        val beginTime = GregorianCalendar(2019, 7, 15, 8, 0)
        val endTime = GregorianCalendar(2019, 7, 15, 9, 30)
        calIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)
        calIntent.putExtra(
            CalendarContract.EXTRA_EVENT_BEGIN_TIME,
            beginTime.timeInMillis
        )
        calIntent.putExtra(
            CalendarContract.EXTRA_EVENT_END_TIME,
            endTime.timeInMillis
        )

        // Recurrency rule (standard iCalendar recurrence rule format (see RFC 5544 for details))
        calIntent.putExtra(CalendarContract.Events.RRULE, "FREQ=WEEKLY;COUNT=10;WKST=SU;BYDAY=TU,TH")
        startActivity(calIntent)

    }

}
