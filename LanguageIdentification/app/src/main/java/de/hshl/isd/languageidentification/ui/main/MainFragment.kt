package de.hshl.isd.languageidentification.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
import de.hshl.isd.languageidentification.R
import de.hshl.isd.languageidentification.infrastructure.adapters.LanguageRecognizerAdapter
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val languageRecognizer = LanguageRecognizerAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        buttonIdLanguage.setOnClickListener {
            languageRecognizer.identifyLanguage(editText.text.toString(),::success,::failure)
        }
        buttonIdAll.setOnClickListener {
            languageRecognizer.identifyPossibleLanguages(editText.text.toString(),::success,::failure)
        }

    }

    private fun success(value: List<Pair<String, Float>>) {
        Snackbar.make(view!!, value.toString(), Snackbar.LENGTH_LONG).show()
    }

    private fun success(value: String) {
        Snackbar.make(view!!, value, Snackbar.LENGTH_LONG).show()
    }

    private fun failure(error: Throwable) {
        Snackbar.make(view!!, R.string.language_id_error, Snackbar.LENGTH_LONG).show()
    }

}