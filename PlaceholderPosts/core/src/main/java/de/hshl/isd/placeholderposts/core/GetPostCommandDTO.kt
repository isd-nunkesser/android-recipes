package de.hshl.isd.placeholderposts.core

data class GetPostCommandDTO(val id: Int)
