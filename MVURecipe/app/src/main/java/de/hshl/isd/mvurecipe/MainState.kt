package de.hshl.isd.mvurecipe

import androidx.compose.getValue
import androidx.compose.mutableStateOf
import androidx.compose.setValue

class MainState {
    var forename by mutableStateOf("")
    var surname by mutableStateOf("")

    fun reset() {
        forename = ""
        surname = ""
    }
}

