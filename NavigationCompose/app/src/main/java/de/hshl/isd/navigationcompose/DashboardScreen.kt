package de.hshl.isd.navigationcompose

import androidx.compose.Composable
import androidx.compose.remember
import androidx.ui.foundation.Text
import androidx.ui.layout.Column
import androidx.ui.material.DrawerState
import androidx.ui.material.Scaffold
import androidx.ui.material.ScaffoldState

@Composable
fun DashboardScreen(scaffoldState: ScaffoldState = remember { ScaffoldState() }) {
    Column {
        Scaffold(
            scaffoldState = scaffoldState,
            drawerContent = {
                AppDrawer(
                    currentScreen = Screen.Home,
                    closeDrawer = {
                        scaffoldState.drawerState = DrawerState.Closed
                    }
                )
            },
            topBar = {
                DrawerTopAppBar(
                    title = "Dashboard Screen",
                    scaffoldState = scaffoldState
                )
            },
            bodyContent = {
                Text("Dashboard")
            }
        )
    }
}