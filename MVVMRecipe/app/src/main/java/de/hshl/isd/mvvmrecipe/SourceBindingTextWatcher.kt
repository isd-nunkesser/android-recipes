package de.hshl.isd.mvvmrecipe

import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.MutableLiveData

class SourceBindingTextWatcher(var source: MutableLiveData<String>) :
    TextWatcher {
    override fun afterTextChanged(p0: Editable?) {
        source.value = p0.toString()
    }

    override fun beforeTextChanged(
        p0: CharSequence?,
        p1: Int,
        p2: Int,
        p3: Int
    ) {
        // Nothing to do
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        // Nothing to do
    }

}

