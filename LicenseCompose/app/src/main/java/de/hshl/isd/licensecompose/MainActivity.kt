package de.hshl.isd.licensecompose

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.ui.core.setContent
import androidx.ui.foundation.Text
import androidx.ui.material.Button
import androidx.ui.material.MaterialTheme
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                Button(onClick = {
                    startActivity(
                            Intent(this,
                                    OssLicensesMenuActivity::class.java))
                }) {
                    Text(text = "Show licenses")
                }
            }
        }
    }
}