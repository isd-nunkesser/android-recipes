package de.hshl.isd.mlkitsmartreply

import com.google.firebase.ml.naturallanguage.FirebaseNaturalLanguage
import com.google.firebase.ml.naturallanguage.smartreply.FirebaseTextMessage
import com.google.firebase.ml.naturallanguage.smartreply.SmartReplySuggestion
import de.hshl.isd.basiccleanarch.Response
import kotlinx.coroutines.tasks.await

class MLKitGateway {
    val smartReply = FirebaseNaturalLanguage
        .getInstance().smartReply

    suspend fun suggestReplies(messages: List<FirebaseTextMessage>): Response {
        try {
            val replies = smartReply.suggestReplies(messages).await()
            return Response.Success<List<SmartReplySuggestion>>(replies.suggestions)
        } catch (e: Exception) {
            return Response.Failure(e)
        }
    }

}