package de.hshl.isd.viewmodelrecipe

import androidx.lifecycle.Observer
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Test

import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.junit.Before

@RunWith(AndroidJUnit4::class)
class ViewModelUnitTest {

    private var viewModel: MainViewModel = MainViewModel()

    @Mock
    lateinit var observer: Observer<String>

    @Before
    fun initMocks() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun increase_isCorrect() {
        viewModel.label.observeForever(observer)
        viewModel.increase()
        verify(observer).onChanged("1")
    }
}
