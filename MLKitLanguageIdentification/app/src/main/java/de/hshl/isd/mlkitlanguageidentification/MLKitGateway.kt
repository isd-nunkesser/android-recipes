package de.hshl.isd.mlkitlanguageidentification

import com.google.firebase.ml.naturallanguage.FirebaseNaturalLanguage
import com.google.firebase.ml.naturallanguage.languageid.IdentifiedLanguage
import de.hshl.isd.basiccleanarch.Response
import kotlinx.coroutines.tasks.await

class MLKitGateway {
    val languageIdentification = FirebaseNaturalLanguage
        .getInstance().languageIdentification

    suspend fun identifyLanguage(text: String): Response {
        try {
            val language = languageIdentification
                .identifyLanguage(text).await()
            return Response.Success<String>(language)
        } catch (e: Exception) {
            return Response.Failure(e)
        }
    }

    suspend fun identifyPossibleLanguages(text: String): Response {
        try {
            val languages = languageIdentification
                .identifyPossibleLanguages(text).await()
            if (languages.isEmpty()) return Response.Failure(java.lang.Exception("No languages identified"))
            return Response.Success<List<IdentifiedLanguage>>(languages)
        } catch (e: Exception) {
            return Response.Failure(e)
        }
    }

}