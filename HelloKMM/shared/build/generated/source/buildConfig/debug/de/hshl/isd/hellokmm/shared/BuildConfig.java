/**
 * Automatically generated file. DO NOT MODIFY
 */
package de.hshl.isd.hellokmm.shared;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "de.hshl.isd.hellokmm.shared";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
