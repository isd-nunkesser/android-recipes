package de.hshl.isd.minimalcleanarch

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import de.hshl.isd.basiccleanarch.Displayer
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), Displayer<String> {

    private val tag = "MainActivity"

    var interactor = MinimalInteractor(MinimalPresenter())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            interactor.execute(null, this)
        }
    }

    override fun display(success: String, requestCode: Int) {
        label.text = success
    }

    override fun display(error: Throwable) {
        Log.e(tag, error.localizedMessage)
    }

}
