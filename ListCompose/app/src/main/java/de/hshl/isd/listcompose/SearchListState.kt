package de.hshl.isd.listcompose

import androidx.compose.getValue
import androidx.compose.mutableStateOf
import androidx.compose.setValue

class SearchListState(var items: List<ItemViewModel>) {
    var filter by mutableStateOf("")
    var filteredItems: List<ItemViewModel> = listOf()
        get() {
            if (filter.isNullOrBlank()) return items
            return items.filter {
                it.title.contains(
                    filter,
                    ignoreCase = true
                ) || it.subtitle.contains(filter, ignoreCase = true)
            }
        }

}

