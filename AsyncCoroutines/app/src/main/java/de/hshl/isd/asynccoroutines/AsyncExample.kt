package de.hshl.isd.asynccoroutines

import android.util.Log
import kotlinx.coroutines.*

class AsyncExample {

    private val tag = "AsyncExample"

    fun backgroundExample() {
        MainScope().launch {
            // launch a new coroutine in background and continue
            withContext(Dispatchers.IO) {
                delay(1000L) // non-blocking delay for 1 second
                Log.i(tag, "World!") // log after delay
            }
        }
        Log.i(tag, "Hello,") // main thread continues while coroutine is delayed
    }

    fun uiExample(handler: (String) -> Unit) {
        val scope = MainScope()
        scope.launch {
            var result: Int
            val one = scope.async { doSomethingUsefulOne() }
            val two = scope.async { doSomethingUsefulTwo() }
            handler("""${one.isCompleted}, ${two.isCompleted}""")
            withContext(Dispatchers.IO) {
                result = one.await() + two.await()
                withContext(Dispatchers.Main) {
                    handler("""$result""")
                }
            }
        }
    }

    suspend fun doSomethingUsefulOne(): Int {
        delay(1000L) // pretend we are doing something useful here
        return 13
    }

    suspend fun doSomethingUsefulTwo(): Int {
        delay(1000L) // pretend we are doing something useful here, too
        return 29
    }

}