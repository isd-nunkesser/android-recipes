package de.hshl.isd.helloworld.ui.main

import android.arch.lifecycle.ViewModel
import de.hshl.isd.helloworld.R

class MainViewModel : ViewModel() {
    val data = R.string.label_greeting
}
