package de.hshl.isd.placeholderposts.core

data class PostEntity(
    val userID: Long,
    val id: Long,
    val title: String,
    val body: String
) {
    override fun toString(): String {
        return "userID=$userID, id=$id, title='$title', body='$body'"
    }
}
