package de.hshl.isd.placeholderposts.infrastructure

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface JSONPlaceholderAPI {

    @GET("posts/{id}")
    fun readPost(@Path("id") id: Int): Call<Post>

    @GET("posts")
    fun readAllPosts(): Call<List<Post>>

    @POST("posts")
    fun createPost(@Body post: Post): Call<Post>

}

